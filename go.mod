module redditclone

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.4.4
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/qur/withmock v0.0.0-20180804144346-b0df8b0932f1 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/twharmon/gouid v0.1.0
	github.com/vektra/mockery/v2 v2.3.0 // indirect
	go.mongodb.org/mongo-driver v1.4.3
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	honnef.co/go/tools v0.0.1-2019.2.3
)
