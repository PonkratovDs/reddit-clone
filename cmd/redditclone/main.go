package main

import (
	"database/sql"
	"html/template"
	"net/http"
	"redditclone/pkg/handlers"
	"redditclone/pkg/items"
	"redditclone/pkg/middleware"
	"redditclone/pkg/session"
	"redditclone/pkg/user"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"gopkg.in/mgo.v2"

	_ "github.com/go-sql-driver/mysql"
	_ "go.mongodb.org/mongo-driver/mongo"
)

func main() {
	templates := template.Must(template.ParseFiles("./template/index.html"))

	zapLogger, _ := zap.NewProduction()
	defer zapLogger.Sync()
	logger := zapLogger.Sugar()

	dsnSQL := "root:love@tcp(localhost:3306)/golang?&charset=utf8&interpolateParams=true"
	dbSQL, err := sql.Open("mysql", dsnSQL)
	if err != nil {
		logger.Error("error while creating database MySQL")
		return
	}

	dbSQL.SetMaxOpenConns(10)

	err = dbSQL.Ping()
	if err != nil {
		logger.Error("error on first access to the database MySQL")
		return
	}

	sess, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		logger.Error("error while creating database MongoDB")
		return
	}

	dbHelper := items.NewDatabaseHelper(sess.DB("coursera"))

	sm := session.NewSessionsMem(dbSQL)
	userRepo := user.NewUserRepo(dbSQL)
	itemsRepo := items.NewItemsRepo(dbHelper)

	handler := &handlers.Handler{
		Tmpl:      templates,
		ItemsRepo: itemsRepo,
		Logger:    logger,
	}

	userHandler := &handlers.UserHandler{
		Tmpl:     templates,
		UserRepo: userRepo,
		Logger:   logger,
		Sessions: sm,
	}

	staticHandler := http.StripPrefix(
		"/static/",
		http.FileServer(http.Dir("./template/static")),
	)
	r := mux.NewRouter()
	r.PathPrefix("/static/").Handler(staticHandler).Name("Static")
	r.HandleFunc("/", handler.Index).Name("Index")
	r.HandleFunc("/manifest.json", func(w http.ResponseWriter,
		r *http.Request) {
		templ := template.Must(template.ParseFiles("./template/manifest.json"))
		err := templ.ExecuteTemplate(w, "manifest.json", nil)
		if err != nil {
			http.Error(w, `Template errror`, http.StatusInternalServerError)
			return
		}
	}).Name("manifest.json")
	r.HandleFunc("/api/register", userHandler.SignUp).Methods("POST").Name("SignUp")
	r.HandleFunc("/api/login", userHandler.Login).Methods("POST").Name("Login")
	r.HandleFunc("/api/posts/", handler.AllPosts).Methods("GET").Name("AllPosts")
	r.HandleFunc("/api/posts", handler.AddPost).Methods("POST").Name("AddPost") //"auth" ok
	r.HandleFunc("/api/post/{id}", handler.GetPostByID).Methods("GET").Name("GetPostByID")
	r.HandleFunc("/api/posts/{category}", handler.GetAllPostsByCategory).Methods("GET").Name("GetAllPostsByCategory")
	r.HandleFunc("/api/post/{id}", handler.AddCommentByPostID).Methods("POST").Name("AddCommentByPostID")                                          //"auth" ok
	r.HandleFunc("/api/post/{id}/{comment_id}", handler.DeleteCommentByPostAndCommentID).Methods("DELETE").Name("DeleteCommentByPostAndCommentID") //"auth" ok
	r.HandleFunc("/api/post/{id}/upvote", handler.MakeUpvoteForPostByID).Methods("GET").Name("MakeUpvoteForPostByID")                              //"auth" ok
	r.HandleFunc("/api/post/{id}/downvote", handler.MakeDownvoteForPostByID).Methods("GET").Name("MakeDownvoteForPostByID")                        //"auth" ok
	//r.HandleFunc("/api/post/{id}/unvote", handler.MakeUnvoteForPostByID).Methods("GET")  //есть, как функционал на сайте. А в ТЗ нет :)
	r.HandleFunc("/api/post/{id}", handler.DeletePostByPostID).Methods("DELETE").Name("DeletePostByPostID") //"auth" ok
	r.HandleFunc("/api/user/{user_login}", handler.AllPostsByUserName).Methods("GET").Name("AllPostsByUserName")

	amw := middleware.AuthenticationMiddleware{
		Logger: logger,
		Sm:     sm,
	}

	lm := middleware.LoggerMiddleware{
		Logger: logger,
	}

	r.Use(amw.Auth, lm.AccessLog, middleware.Panic)

	addr := ":8080"
	logger.Infow("starting server",
		"type", "START",
		"addr", addr,
	)
	http.ListenAndServe(addr, r)
}
