COMMIT?=$(shell git rev-parse --short HEAD)
BUILD_TIME?=$(shell date -u '+%Y-%m-%d_%H:%M:%S')

export GO111MODULE=on

.PHONY: build
build:
	@echo "-- building binary"
	go build \
		-ldflags "-X main.buildHash=${COMMIT} -X main.buildTime=${BUILD_TIME}" \
		-o ./bin/redditclone \
		./cmd/redditclone/main.go

.PHONY: test
test:
	@echo "-- run tests"
	go test -v -coverpkg=./... ./...

.PHONY: dc
dc:
	@echo "-- starting docker compose"
	docker-compose -f ./db/docker-compose.yml up
