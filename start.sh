#!/bin/bash

echo -n "clear doc? y/n "

read clearDoc

if [[ "$clearDoc" == "y" ]]
then
    docker rm $(docker ps -a -q) && docker volume prune -f
fi

cd ./db && docker-compose up