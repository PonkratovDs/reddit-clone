package handlers_test

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http/httptest"
	"redditclone/pkg/handlers"
	"redditclone/pkg/handlers/mocks"
	"redditclone/pkg/session"
	"redditclone/pkg/user"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

//для не успешного чтения можно просто сделать свой ридер, который будет пробрасывать ошибку
type myReader struct{}

func (mr *myReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("mock_error")
}

func (mr *myReader) Close() error {
	return nil
}

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoicXdlcnR5MTIzNDU2NzgiLCJpZCI6IjVmYmZjYjI5ZjM0YjI1MDAwN2FmZjBhMSJ9LCJpYXQiOjE2MDY0NjU4ODUsImV4cCI6MTYwNzA3MDY4NX0.ceUjwFKDPOFzkl2Lc0EOBP1ObvYU-toNHA4T2LT2s3I"

func TestLogin(t *testing.T) {
	var userRepoHelperErrBadPass handlers.UserRepositoryInterface
	var userRepoHelperErrNoUser handlers.UserRepositoryInterface
	var userRepoHelperErrAuthorize handlers.UserRepositoryInterface
	var userRepoHelper handlers.UserRepositoryInterface
	var sessionManagerHelper handlers.SessionManagerInterface
	var sessionManagerHelperErr handlers.SessionManagerInterface

	userRepoHelperErrBadPass = &mocks.UserRepositoryInterface{}
	userRepoHelperErrNoUser = &mocks.UserRepositoryInterface{}
	userRepoHelperErrAuthorize = &mocks.UserRepositoryInterface{}
	userRepoHelper = &mocks.UserRepositoryInterface{}
	sessionManagerHelper = &mocks.SessionManagerInterface{}
	sessionManagerHelperErr = &mocks.SessionManagerInterface{}

	body := ioutil.NopCloser(bytes.NewBufferString(`{"username": "qwerty12345678", "password": "passpass"}`))
	req := httptest.NewRequest("POST", "/api/login", body)
	w := httptest.NewRecorder()

	userRepoHelperErrBadPass.(*mocks.UserRepositoryInterface).On("Authorize", "qwerty12345678", "passpass").Return(nil, user.ErrBadPass)

	service := &handlers.UserHandler{
		Tmpl:     template.Must(template.ParseGlob("../../template/index.html")),
		Logger:   zap.NewNop().Sugar(),
		UserRepo: userRepoHelperErrBadPass,
		Sessions: sessionManagerHelper,
	}

	service.Login(w, req)

	resp := w.Result()

	assert.Equal(t, resp.Status, "401 Unauthorized")

	userRepoHelperErrNoUser.(*mocks.UserRepositoryInterface).On("Authorize", "qwerty12345678", "passpass").Return(nil, user.ErrNoUser)
	body = ioutil.NopCloser(bytes.NewBufferString(`{"username": "qwerty12345678", "password": "passpass"}`))
	req = httptest.NewRequest("POST", "/api/login", body)
	w = httptest.NewRecorder()
	service.UserRepo = userRepoHelperErrNoUser

	service.Login(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "401 Unauthorized")

	userRepoHelperErrAuthorize.(*mocks.UserRepositoryInterface).On("Authorize", "qwerty12345678", "passpass").Return(nil, errors.New("mock_error"))
	body = ioutil.NopCloser(bytes.NewBufferString(`{"username": "qwerty12345678", "password": "passpass"}`))
	req = httptest.NewRequest("POST", "/api/login", body)
	w = httptest.NewRecorder()
	service.UserRepo = userRepoHelperErrAuthorize

	service.Login(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	userRepoHelper.(*mocks.UserRepositoryInterface).On("Authorize", "qwerty12345678", "passpass").Return(&user.User{UserName: "qwerty12345678", ID: "1"}, nil)
	w = httptest.NewRecorder()
	sessionManagerHelper.(*mocks.SessionManagerInterface).On("Create", w, "1", "qwerty12345678").Return(&session.Session{ID: token, UserID: "1", UserName: "qwerty12345678"}, nil)
	body = ioutil.NopCloser(bytes.NewBufferString(`{"username": "qwerty12345678", "password": "passpass"}`))
	req = httptest.NewRequest("POST", "/api/login", body)
	service.UserRepo = userRepoHelper
	service.Sessions = sessionManagerHelper

	service.Login(w, req)

	resp = w.Result()
	bodyResp, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, resp.Status, "200 OK")
	assert.Equal(t, []byte(fmt.Sprintf(`{"token":"%s"}`, token)), bodyResp)

	w = httptest.NewRecorder()
	sessionManagerHelperErr.(*mocks.SessionManagerInterface).On("Create", w, "1", "qwerty12345678").Return(nil, errors.New("mock_error"))
	body = ioutil.NopCloser(bytes.NewBufferString(`{"username": "qwerty12345678", "password": "passpass"}`))
	req = httptest.NewRequest("POST", "/api/login", body)
	service.Sessions = sessionManagerHelperErr

	service.Login(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	body = &myReader{}
	req = httptest.NewRequest("POST", "/api/login", body)
	w = httptest.NewRecorder()

	service.Login(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "400 Bad Request")
}

func TestSignUp(t *testing.T) {
	var userRepoHelper handlers.UserRepositoryInterface
	var userRepoHelperErrAddUser handlers.UserRepositoryInterface
	var sessionManagerHelper handlers.SessionManagerInterface
	var sessionManagerHelperErrCreate handlers.SessionManagerInterface

	userRepoHelper = &mocks.UserRepositoryInterface{}
	userRepoHelperErrAddUser = &mocks.UserRepositoryInterface{}
	sessionManagerHelper = &mocks.SessionManagerInterface{}
	sessionManagerHelperErrCreate = &mocks.SessionManagerInterface{}

	body := ioutil.NopCloser(bytes.NewBufferString(`{bad_js}`))
	req := httptest.NewRequest("POST", "/api/register", body)
	w := httptest.NewRecorder()

	service := &handlers.UserHandler{
		Tmpl:     template.Must(template.ParseGlob("../../template/index.html")),
		Logger:   zap.NewNop().Sugar(),
		UserRepo: userRepoHelper,
		Sessions: sessionManagerHelper,
	}

	service.SignUp(w, req)

	resp := w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	userRepoHelper.(*mocks.UserRepositoryInterface).On("AddUser", "qwerty12345678", "passpass").Return("1", nil)
	w = httptest.NewRecorder()
	sessionManagerHelper.(*mocks.SessionManagerInterface).On("Create", w, "1", "qwerty12345678").Return(&session.Session{ID: token, UserID: "1", UserName: "qwerty12345678"}, nil)
	body = ioutil.NopCloser(bytes.NewBufferString(`{"username": "qwerty12345678", "password": "passpass"}`))
	req = httptest.NewRequest("POST", "/api/login", body)
	service.UserRepo = userRepoHelper
	service.Sessions = sessionManagerHelper

	service.SignUp(w, req)

	resp = w.Result()
	bodyResp, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, resp.Status, "201 Created")
	assert.Equal(t, []byte(fmt.Sprintf(`{"token":"%s"}`, token)), bodyResp)

	userRepoHelperErrAddUser.(*mocks.UserRepositoryInterface).On("AddUser", "qwerty12345678", "passpass").Return("", errors.New("mock_error"))
	w = httptest.NewRecorder()
	body = ioutil.NopCloser(bytes.NewBufferString(`{"username": "qwerty12345678", "password": "passpass"}`))
	req = httptest.NewRequest("POST", "/api/login", body)
	service.UserRepo = userRepoHelperErrAddUser

	service.SignUp(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	w = httptest.NewRecorder()
	sessionManagerHelperErrCreate.(*mocks.SessionManagerInterface).On("Create", w, "1", "qwerty12345678").Return(nil, errors.New("mock_error"))
	body = ioutil.NopCloser(bytes.NewBufferString(`{"username": "qwerty12345678", "password": "passpass"}`))
	req = httptest.NewRequest("POST", "/api/login", body)
	service.UserRepo = userRepoHelper
	service.Sessions = sessionManagerHelperErrCreate

	service.SignUp(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")
}
