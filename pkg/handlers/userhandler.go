package handlers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"redditclone/pkg/session"
	"redditclone/pkg/user"

	"go.uber.org/zap"
)

type SessionManagerInterface interface {
	Create(w http.ResponseWriter, userID string, userName string) (*session.Session, error)
}
type UserRepositoryInterface interface {
	Authorize(username string, pass string) (*user.User, error)
	AddUser(username string, pass string) (string, error)
}

type UserHandler struct {
	Tmpl     *template.Template
	Logger   *zap.SugaredLogger
	UserRepo UserRepositoryInterface
	Sessions SessionManagerInterface
}

func (h *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	unmUser, err := ReadBody(w, r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Error(err)
		return
	}

	u, err := h.UserRepo.Authorize(unmUser.UserName, unmUser.Password)
	switch err {
	case nil:
		break
	case user.ErrNoUser:
		w.WriteHeader(http.StatusUnauthorized)
		h.Logger.Infof("user with this login %s does not exist", unmUser.UserName)
		return
	case user.ErrBadPass:
		w.WriteHeader(http.StatusUnauthorized)
		h.Logger.Infof("a user with this login %s entered an incorrect password", unmUser.UserName)
		return
	default:
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("server error: %w", err)
		return
	}

	sess, err := h.Sessions.Create(w, u.ID, u.UserName)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("failed to set a session for this user %s. Mistake: %s", unmUser.UserName, err)
		return
	}

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, fmt.Sprintf(`{"token":"%s"}`, sess.ID))
}

func ReadBody(w http.ResponseWriter, r *http.Request) (*user.UnmarshalUser, error) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return nil, fmt.Errorf("%s. Error: %w", ErrBody, err)
	}

	u := new(user.UnmarshalUser)
	err = json.Unmarshal(body, u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return nil, fmt.Errorf("%s. Error: %w", ErrBadJS, err)
	}
	return u, nil
}

func (h *UserHandler) SignUp(w http.ResponseWriter, r *http.Request) {
	u, err := ReadBody(w, r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Error(err)
		return
	}
	id, err := h.UserRepo.AddUser(u.UserName, u.Password)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Error(err)
		return
	}

	sess, err := h.Sessions.Create(w, id, u.UserName)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Error(err)
		return
	}

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	io.WriteString(w, fmt.Sprintf(`{"token":"%s"}`, sess.ID))
}
