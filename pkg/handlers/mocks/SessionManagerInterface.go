// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	http "net/http"
	session "redditclone/pkg/session"

	mock "github.com/stretchr/testify/mock"
)

// SessionManagerInterface is an autogenerated mock type for the SessionManagerInterface type
type SessionManagerInterface struct {
	mock.Mock
}

// Create provides a mock function with given fields: w, userID, userName
func (_m *SessionManagerInterface) Create(w http.ResponseWriter, userID string, userName string) (*session.Session, error) {
	ret := _m.Called(w, userID, userName)

	var r0 *session.Session
	if rf, ok := ret.Get(0).(func(http.ResponseWriter, string, string) *session.Session); ok {
		r0 = rf(w, userID, userName)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*session.Session)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(http.ResponseWriter, string, string) error); ok {
		r1 = rf(w, userID, userName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
