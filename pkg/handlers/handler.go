package handlers

import (
	"encoding/json"
	"errors"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"redditclone/pkg/comment"
	"redditclone/pkg/items"
	"redditclone/pkg/session"
	"redditclone/pkg/user"
	"redditclone/pkg/vote"

	"github.com/gorilla/mux"
	"github.com/twharmon/gouid"
	"go.uber.org/zap"
)

var (
	ErrBody  = errors.New("error while reading request body")
	ErrBadJS = errors.New("error when unpacking json")
)

type HandlerRepositoryInterface interface {
	GetAllItems() []*items.Item
	AddItem(item *items.Item) error
	GetItemByID(id string) (*items.Item, error)
	UpdateItem(item *items.Item) error
	GetAllItemsByCategory(category string) []*items.Item
	AddCommentsByID(id string, comment comment.Comment) error
	DeleteCommentByPostAndCommentID(id string, commentID string) error
	AddVoteByID(id string, vote vote.Vote) error
	CalcUpvotePercentAndScorePostByID(id string) error
	DeleteItemByItemID(id string) error
	GetAllItemsByUserName(userName string) []*items.Item
}

type Handler struct {
	Tmpl      *template.Template
	Logger    *zap.SugaredLogger
	ItemsRepo HandlerRepositoryInterface
}

var Static = "index.html"

var IsTest = false

func (h *Handler) Index(w http.ResponseWriter, r *http.Request) {
	err := h.Tmpl.ExecuteTemplate(w, Static, nil)
	if err != nil {
		http.Error(w, `Template error`, http.StatusInternalServerError)
		return
	}
}

func (h *Handler) AllPosts(w http.ResponseWriter, r *http.Request) {
	items := h.ItemsRepo.GetAllItems()
	itemsJS, err := json.Marshal(items)
	if err != nil || string(itemsJS) == "null" { //если пустой список, то вернулось бы "[]", а не "null"
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(itemsJS)
}

func (h *Handler) AddPost(w http.ResponseWriter, r *http.Request) {
	sess, err := session.SessionFromContext(r.Context())
	if err != nil {
		h.Logger.Infof("error while getting context: %w", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Infof("%s. Error: %w", ErrBody, err)
		return
	}
	unmItem := new(items.UnmarshalItem)
	err = json.Unmarshal(body, unmItem)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	//id := gouid.String(24)
	curVote := vote.Vote{
		UserID: sess.UserID,
		Vote:   1,
	}
	item := &items.Item{
		Author: user.User{
			UserName: sess.UserName,
			ID:       sess.UserID,
		},
		Category: unmItem.Category,
		Comments: []comment.Comment{},
		Created:  time.Now().Format(time.RFC3339),
		//	ItemID:           id,
		Score:            1,
		Text:             unmItem.Text,
		Title:            unmItem.Title,
		Type:             unmItem.Type,
		UpVotePercentage: 100,
		URL:              unmItem.URL,
		Views:            0,
		Votes:            []vote.Vote{curVote},
	}
	err = h.ItemsRepo.AddItem(item)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("error: %w", err)
		return
	}

	if IsTest { //если плохо, то есть решение возвращать item в AddItem, чтобы его заменить. К сожалению, через мок его заменить не получается
		item = nil
	}
	itemJS, err := json.Marshal(item)
	if err != nil || string(itemJS) == "null" {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(itemJS)
}

func (h *Handler) GetPostByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, ok := vars["id"]; !ok {
		w.WriteHeader(http.StatusBadGateway)
		h.Logger.Info("error while getting id post")
		return
	}
	id := vars["id"]
	item, err := h.ItemsRepo.GetItemByID(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Infof("%s. Error: %w", items.ErrNoItem, err)
		return
	}
	item.Views++
	err = h.ItemsRepo.UpdateItem(item)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("Error update item. Error: %w", err)
		return
	}
	if _, ok := vars["test"]; ok {
		item = nil
	}
	itemJS, err := json.Marshal(item)
	if err != nil || string(itemJS) == "null" {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(itemJS)
}

func (h *Handler) GetAllPostsByCategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, ok := vars["category"]; !ok {
		w.WriteHeader(http.StatusBadGateway)
		h.Logger.Info("error while getting category post")
		return
	}
	category := vars["category"]
	items := h.ItemsRepo.GetAllItemsByCategory(category)
	itemsJS, err := json.Marshal(items)
	if err != nil || string(itemsJS) == "null" {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(itemsJS)
}

func (h *Handler) AddCommentByPostID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	sess, err := session.SessionFromContext(r.Context())
	if err != nil {
		h.Logger.Infof("error while getting context: %w", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Infof("%s. Error: %w", ErrBody, err)
		return
	}
	unmComment := new(comment.UnmarshalComment)
	err = json.Unmarshal(body, unmComment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	if _, ok := vars["id"]; !ok {
		w.WriteHeader(http.StatusBadGateway)
		h.Logger.Info("error while getting id post")
		return
	}
	idPost := vars["id"]
	idComment := gouid.String(24)
	author := user.User{
		UserName: sess.UserName,
		ID:       sess.UserID,
	}
	comment := comment.Comment{
		Author:  author,
		Body:    unmComment.Comment,
		Created: time.Now().Format(time.RFC3339),
		ID:      idComment,
	}
	err = h.ItemsRepo.AddCommentsByID(idPost, comment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Info("failed to send a comment")
		return
	}
	copyItem, err := h.ItemsRepo.GetItemByID(idPost)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Infof("%s. Error: %w", items.ErrNoItem, err)
		return
	}
	itemJS, err := json.Marshal(copyItem)
	if err != nil || string(itemJS) == "null" {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(itemJS)
}

func (h *Handler) DeleteCommentByPostAndCommentID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	_, okID := vars["id"]
	_, okCommentID := vars["comment_id"]
	if !okID || !okCommentID {
		w.WriteHeader(http.StatusBadGateway)
		h.Logger.Info("error while getting id post and comment id")
		return
	}
	id, commentID := vars["id"], vars["comment_id"]
	err := h.ItemsRepo.DeleteCommentByPostAndCommentID(id, commentID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("error delete comment. Error: %w", err)
		return
	}
	copyItem, err := h.ItemsRepo.GetItemByID(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Infof("%s. Error: %w", items.ErrNoItem, err)
		return
	}
	itemJS, err := json.Marshal(copyItem)
	if err != nil || string(itemJS) == "null" {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(itemJS)
}

func (h *Handler) RatePost(w http.ResponseWriter, r *http.Request, voteVal int) {
	sess, err := session.SessionFromContext(r.Context())
	if err != nil {
		h.Logger.Infof("error while getting context: %w", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	vars := mux.Vars(r)
	if _, ok := vars["id"]; !ok {
		w.WriteHeader(http.StatusBadGateway)
		h.Logger.Info("error while getting id post")
		return
	}
	idPost := vars["id"]
	vote := vote.Vote{
		UserID: sess.UserID,
		Vote:   voteVal,
	}
	err = h.ItemsRepo.AddVoteByID(idPost, vote)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Info("failed to send a vote")
		return
	}
	err = h.ItemsRepo.CalcUpvotePercentAndScorePostByID(idPost)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Info("failed to calc a percent and score upvote")
		return
	}
	copyItem, err := h.ItemsRepo.GetItemByID(idPost)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Infof("%s. Error: %w", items.ErrNoItem, err)
		return
	}
	itemJS, err := json.Marshal(copyItem)
	if err != nil || string(itemJS) == "null" {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(itemJS)
}

func (h *Handler) MakeUpvoteForPostByID(w http.ResponseWriter, r *http.Request) {
	h.RatePost(w, r, 1)
}

func (h *Handler) MakeDownvoteForPostByID(w http.ResponseWriter, r *http.Request) {
	h.RatePost(w, r, -1)
}

// func (h *Handler) MakeUnvoteForPostByID(w http.ResponseWriter, r *http.Request) {
// 	h.RatePost(w, r, 0)
// }

func (h *Handler) DeletePostByPostID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, ok := vars["id"]; !ok {
		w.WriteHeader(http.StatusBadGateway)
		h.Logger.Info("error while getting id post")
		return
	}
	idPost := vars["id"]
	err := h.ItemsRepo.DeleteItemByItemID(idPost)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Infof("error when trying to delete a post. wrong id post, error: %s", err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, `{"message":"success"}`)
}

func (h *Handler) AllPostsByUserName(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, ok := vars["user_login"]; !ok {
		w.WriteHeader(http.StatusBadGateway)
		h.Logger.Info("error while getting user login")
		return
	}
	userName := vars["user_login"]
	items := h.ItemsRepo.GetAllItemsByUserName(userName)
	itemsJS, err := json.Marshal(items)
	if err != nil || string(itemsJS) == "null" {
		w.WriteHeader(http.StatusInternalServerError)
		h.Logger.Infof("%s. Error: %w", ErrBadJS, err)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(itemsJS)
}
