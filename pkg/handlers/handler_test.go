package handlers_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"html/template"
	"io"
	"io/ioutil"
	"net/http/httptest"
	"redditclone/pkg/comment"
	"redditclone/pkg/handlers"
	"redditclone/pkg/handlers/mocks"
	"redditclone/pkg/items"
	"redditclone/pkg/session"
	"redditclone/pkg/user"
	"redditclone/pkg/vote"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.uber.org/zap"
)

var sess = &session.Session{
	ID:       token,
	UserID:   "1",
	UserName: "qwerty12345678",
}

var testItem = &items.Item{
	Author:           user.User{UserName: "qwerty12345678", ID: "1"},
	Category:         "music",
	Comments:         []comment.Comment{},
	Created:          time.Now().Format(time.RFC3339),
	ItemID:           "",
	Score:            1,
	Title:            "mock_title",
	Text:             "mock_text",
	Type:             "text",
	UpVotePercentage: 100,
	Views:            0,
	Votes:            []vote.Vote{vote.Vote{UserID: "1", Vote: 1}},
}

var MockError = errors.New("mock_error")

func TestIndex(t *testing.T) {
	req := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	service := &handlers.Handler{
		Tmpl: template.Must(template.ParseGlob("../../template/index.html")),
	}

	service.Index(w, req)

	resp := w.Result()
	assert.Equal(t, resp.Status, "200 OK")

	req = httptest.NewRequest("GET", "/", nil)
	w = httptest.NewRecorder()
	handlers.Static = "bad_name.html"

	service.Index(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")
	handlers.Static = "index.html"
}

func TestAllPosts(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperBadJS handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperBadJS = &mocks.HandlerRepositoryInterface{}
	posts := []*items.Item{
		&items.Item{Title: "mock_1"},
		&items.Item{Title: "mock_2"},
	}

	repoHelper.(*mocks.HandlerRepositoryInterface).On("GetAllItems").Return(posts)

	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}

	req := httptest.NewRequest("GET", "/api/posts/", nil)
	w := httptest.NewRecorder()

	service.AllPosts(w, req)

	resp := w.Result()
	bodyResp, err := ioutil.ReadAll(resp.Body)
	expected, errJS := json.Marshal(posts)

	assert.NoError(t, err)
	assert.NoError(t, errJS)
	assert.Equal(t, resp.Status, "200 OK")
	assert.Equal(t, expected, bodyResp)

	repoHelperBadJS.(*mocks.HandlerRepositoryInterface).On("GetAllItems").Return(nil)
	service.ItemsRepo = repoHelperBadJS

	req = httptest.NewRequest("GET", "/api/posts/", nil)
	w = httptest.NewRecorder()

	service.AllPosts(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")
}

func getCopyTestItem() *items.Item {
	tmp := *testItem
	return &tmp
}

func TestAddPost(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperErr handlers.HandlerRepositoryInterface
	var repoHelperErrMarshal handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperErr = &mocks.HandlerRepositoryInterface{}
	repoHelperErrMarshal = &mocks.HandlerRepositoryInterface{}

	repoHelper.(*mocks.HandlerRepositoryInterface).On("AddItem", mock.AnythingOfType("*items.Item")).Return(nil)

	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}

	body := ioutil.NopCloser(bytes.NewBufferString(`{"category": "music", "type": "text", "title": "mock_title", "text": "mock_text"}`))
	req := httptest.NewRequest("POST", "/api/posts", body)

	ctx := context.WithValue(req.Context(), session.SessionKey, sess)
	w := httptest.NewRecorder()

	service.AddPost(w, req.WithContext(ctx))

	resp := w.Result()
	bodyResp, err := ioutil.ReadAll(resp.Body)

	copyItem := getCopyTestItem()
	copyItem.Created = time.Now().Format(time.RFC3339)
	expected, errJS := json.Marshal(copyItem)

	assert.NoError(t, err)
	assert.NoError(t, errJS)
	assert.Equal(t, resp.Status, "201 Created")
	assert.Equal(t, expected, bodyResp)

	//не кладем контекст
	body = ioutil.NopCloser(bytes.NewBufferString(`{"category": "music", "type": "text", "title": "mock_title", "text": "mock_text"}`))
	req = httptest.NewRequest("POST", "/api/posts", body)
	w = httptest.NewRecorder()

	service.AddPost(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	//ошибка readAll

	body = &myReader{}
	req = httptest.NewRequest("POST", "/api/posts", body)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()

	service.AddPost(w, req.WithContext(ctx))

	resp = w.Result()

	assert.Equal(t, resp.Status, "400 Bad Request")

	//ошибка unmItem

	body = ioutil.NopCloser(bytes.NewBufferString(`{bad_js}`))
	req = httptest.NewRequest("POST", "/api/posts", body)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()

	service.AddPost(w, req.WithContext(ctx))

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	// ошибка AddItem

	body = ioutil.NopCloser(bytes.NewBufferString(`{"category": "music", "type": "text", "title": "mock_title", "text": "mock_text"}`))
	req = httptest.NewRequest("POST", "/api/posts", body)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()

	repoHelperErr.(*mocks.HandlerRepositoryInterface).On("AddItem", mock.AnythingOfType("*items.Item")).Return(MockError)
	service.ItemsRepo = repoHelperErr

	service.AddPost(w, req.WithContext(ctx))

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	//ошибка с Marshal

	body = ioutil.NopCloser(bytes.NewBufferString(`{"category": "music", "type": "text", "title": "mock_title", "text": "mock_text"}`))
	req = httptest.NewRequest("POST", "/api/posts", body)
	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()

	repoHelperErrMarshal.(*mocks.HandlerRepositoryInterface).On("AddItem", mock.AnythingOfType("*items.Item")).Return(nil)
	service.ItemsRepo = repoHelperErrMarshal

	handlers.IsTest = true
	service.AddPost(w, req.WithContext(ctx))
	handlers.IsTest = false

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")
}

func TestGetPostByID(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperErrGetItem handlers.HandlerRepositoryInterface
	var repoHelperErrUpdateItem handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperErrGetItem = &mocks.HandlerRepositoryInterface{}
	repoHelperErrUpdateItem = &mocks.HandlerRepositoryInterface{}

	repoHelper.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(getCopyTestItem(), nil)
	repoHelper.(*mocks.HandlerRepositoryInterface).On("UpdateItem", mock.AnythingOfType("*items.Item")).Return(nil)
	repoHelperErrGetItem.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(nil, MockError)
	repoHelperErrUpdateItem.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(getCopyTestItem(), nil)
	repoHelperErrUpdateItem.(*mocks.HandlerRepositoryInterface).On("UpdateItem", mock.AnythingOfType("*items.Item")).Return(MockError)

	req := httptest.NewRequest("GET", "/api/post/1", nil)
	w := httptest.NewRecorder()
	vars := map[string]string{
		"id": "1",
	}
	req = mux.SetURLVars(req, vars)

	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}

	service.GetPostByID(w, req)

	resp := w.Result()
	bodyResp, err := ioutil.ReadAll(resp.Body)

	copyItem := getCopyTestItem()
	copyItem.Created = time.Now().Format(time.RFC3339)
	copyItem.Views++
	expected, errJS := json.Marshal(copyItem)

	assert.NoError(t, err)
	assert.NoError(t, errJS)
	assert.Equal(t, resp.Status, "200 OK")
	assert.Equal(t, expected, bodyResp)

	helper := func(repo handlers.HandlerRepositoryInterface, status string, vars map[string]string) {
		req = httptest.NewRequest("GET", "/api/post/1", nil)
		w = httptest.NewRecorder()
		req = mux.SetURLVars(req, vars)

		service.ItemsRepo = repo

		service.GetPostByID(w, req)

		resp = w.Result()

		assert.Equal(t, resp.Status, status)
	}

	//error GetItemByID

	helper(repoHelperErrGetItem, "400 Bad Request", map[string]string{"id": "1"})

	//error UpdateItem

	helper(repoHelperErrUpdateItem, "500 Internal Server Error", map[string]string{"id": "1"})

	//error BadJs

	helper(repoHelper, "500 Internal Server Error", map[string]string{"id": "1", "test": "true"})

	//error not id

	helper(repoHelper, "502 Bad Gateway", map[string]string{})
}

func TestGetAllPostsByCategory(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperErrJS handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperErrJS = &mocks.HandlerRepositoryInterface{}

	copyItem := getCopyTestItem()
	copyItem.Category = "mocked"

	repoHelper.(*mocks.HandlerRepositoryInterface).On("GetAllItemsByCategory", "mocked").Return([]*items.Item{copyItem})
	repoHelperErrJS.(*mocks.HandlerRepositoryInterface).On("GetAllItemsByCategory", "mocked").Return(nil)

	req := httptest.NewRequest("GET", "/api/posts/mocked", nil)
	w := httptest.NewRecorder()
	vars := map[string]string{
		"category": "mocked",
	}
	req = mux.SetURLVars(req, vars)

	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}

	service.GetAllPostsByCategory(w, req)

	resp := w.Result()
	bodyResp, err := ioutil.ReadAll(resp.Body)

	expected, errJS := json.Marshal([]*items.Item{copyItem})

	assert.NoError(t, err)
	assert.NoError(t, errJS)
	assert.Equal(t, resp.Status, "200 OK")
	assert.Equal(t, expected, bodyResp)

	helper := func(repo handlers.HandlerRepositoryInterface, status string, vars map[string]string) {
		req = httptest.NewRequest("GET", "/api/posts/mocked", nil)
		w = httptest.NewRecorder()
		req = mux.SetURLVars(req, vars)

		service.ItemsRepo = repo

		service.GetAllPostsByCategory(w, req)

		resp = w.Result()

		assert.Equal(t, resp.Status, status)
	}

	//bad JS

	helper(repoHelperErrJS, "500 Internal Server Error", map[string]string{"category": "mocked"})

	//no category

	helper(repoHelper, "502 Bad Gateway", map[string]string{})
}

func TestAddCommentByPostID(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperErrAddComment handlers.HandlerRepositoryInterface
	var repoHelperErrGetItem handlers.HandlerRepositoryInterface
	var repoHelperBadMarshal handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperErrAddComment = &mocks.HandlerRepositoryInterface{}
	repoHelperErrGetItem = &mocks.HandlerRepositoryInterface{}
	repoHelperBadMarshal = &mocks.HandlerRepositoryInterface{}

	copyItem := getCopyTestItem()
	copyItem.Comments = []comment.Comment{
		comment.Comment{
			Author:  copyItem.Author,
			Body:    "mocked_comment",
			Created: time.Now().Format(time.RFC3339),
			ID:      "1",
		},
	}

	repoHelper.(*mocks.HandlerRepositoryInterface).On("AddCommentsByID", "1", mock.AnythingOfType("comment.Comment")).Return(nil)
	repoHelper.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(copyItem, nil)
	repoHelperErrAddComment.(*mocks.HandlerRepositoryInterface).On("AddCommentsByID", "1", mock.AnythingOfType("comment.Comment")).Return(MockError)
	repoHelperErrGetItem.(*mocks.HandlerRepositoryInterface).On("AddCommentsByID", "1", mock.AnythingOfType("comment.Comment")).Return(nil)
	repoHelperErrGetItem.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(nil, MockError)
	repoHelperBadMarshal.(*mocks.HandlerRepositoryInterface).On("AddCommentsByID", "1", mock.AnythingOfType("comment.Comment")).Return(nil)
	repoHelperBadMarshal.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(nil, nil)

	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}

	body := ioutil.NopCloser(bytes.NewBufferString(`{"comment": "mocked_comment"}`))
	req := httptest.NewRequest("POST", "/api/post/1", body)

	vars := map[string]string{
		"id": "1",
	}
	req = mux.SetURLVars(req, vars)

	ctx := context.WithValue(req.Context(), session.SessionKey, sess)
	w := httptest.NewRecorder()

	service.AddCommentByPostID(w, req.WithContext(ctx))

	resp := w.Result()
	bodyResp, err := ioutil.ReadAll(resp.Body)

	expected, errJS := json.Marshal(copyItem)

	assert.NoError(t, err)
	assert.NoError(t, errJS)
	assert.Equal(t, resp.Status, "200 OK")
	assert.Equal(t, expected, bodyResp)

	//error context
	req = httptest.NewRequest("POST", "/api/post/1", nil)
	w = httptest.NewRecorder()

	service.AddCommentByPostID(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	//error body

	body = &myReader{}
	req = httptest.NewRequest("POST", "/api/post/1", body)

	ctx = context.WithValue(req.Context(), session.SessionKey, sess)
	w = httptest.NewRecorder()

	service.AddCommentByPostID(w, req.WithContext(ctx))

	resp = w.Result()

	assert.Equal(t, resp.Status, "400 Bad Request")

	helper := func(repo handlers.HandlerRepositoryInterface, status string, body io.ReadCloser, vars map[string]string) {
		req = httptest.NewRequest("POST", "/api/post/1", body)
		req = mux.SetURLVars(req, vars)
		ctx = context.WithValue(req.Context(), session.SessionKey, sess)
		w := httptest.NewRecorder()
		service.ItemsRepo = repo

		service.AddCommentByPostID(w, req.WithContext(ctx))

		resp = w.Result()

		assert.Equal(t, resp.Status, status)
	}

	//err unmarshal comment

	helper(repoHelper, "500 Internal Server Error", ioutil.NopCloser(bytes.NewBufferString(`{bad_body}`)), map[string]string{"id": "1"})

	//err no id

	helper(repoHelper, "502 Bad Gateway", ioutil.NopCloser(bytes.NewBufferString(`{"comment": "mocked_comment"}`)), map[string]string{})

	//err AddComentByID

	helper(repoHelperErrAddComment, "500 Internal Server Error", ioutil.NopCloser(bytes.NewBufferString(`{"comment": "mocked_comment"}`)), map[string]string{"id": "1"})

	//err GetItemByID
	helper(repoHelperErrGetItem, "400 Bad Request", ioutil.NopCloser(bytes.NewBufferString(`{"comment": "mocked_comment"}`)), map[string]string{"id": "1"})

	//err marshal

	helper(repoHelperBadMarshal, "500 Internal Server Error", ioutil.NopCloser(bytes.NewBufferString(`{"comment": "mocked_comment"}`)), map[string]string{"id": "1"})
}

func TestDeleteCommentByPostAndCommentID(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperErrDelete handlers.HandlerRepositoryInterface
	var repoHelperErrGetItem handlers.HandlerRepositoryInterface
	var repoHelperBadMarshal handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperErrDelete = &mocks.HandlerRepositoryInterface{}
	repoHelperErrGetItem = &mocks.HandlerRepositoryInterface{}
	repoHelperBadMarshal = &mocks.HandlerRepositoryInterface{}

	copyItem := getCopyTestItem()

	repoHelper.(*mocks.HandlerRepositoryInterface).On("DeleteCommentByPostAndCommentID", "1", "comment_id").Return(nil)
	repoHelper.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(copyItem, nil)
	repoHelperErrDelete.(*mocks.HandlerRepositoryInterface).On("DeleteCommentByPostAndCommentID", "1", "comment_id").Return(MockError)
	repoHelperErrGetItem.(*mocks.HandlerRepositoryInterface).On("DeleteCommentByPostAndCommentID", "1", "comment_id").Return(nil)
	repoHelperErrGetItem.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(nil, MockError)
	repoHelperBadMarshal.(*mocks.HandlerRepositoryInterface).On("DeleteCommentByPostAndCommentID", "1", "comment_id").Return(nil)
	repoHelperBadMarshal.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(nil, nil)

	req := httptest.NewRequest("DELETE", "/api/post/1/comment_id}", nil)
	vars := map[string]string{
		"id":         "1",
		"comment_id": "comment_id",
	}
	req = mux.SetURLVars(req, vars)
	w := httptest.NewRecorder()
	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}

	service.DeleteCommentByPostAndCommentID(w, req)

	resp := w.Result()
	assert.Equal(t, resp.Status, "200 OK")

	helper := func(repo handlers.HandlerRepositoryInterface, status string, vars map[string]string) {
		req = httptest.NewRequest("DELETE", "/api/post/1/comment_id}", nil)
		req = mux.SetURLVars(req, vars)
		w = httptest.NewRecorder()
		service.ItemsRepo = repo

		service.DeleteCommentByPostAndCommentID(w, req)

		resp = w.Result()
		assert.Equal(t, resp.Status, status)
	}

	helper(repoHelper, "502 Bad Gateway", map[string]string{})
	helper(repoHelperErrDelete, "500 Internal Server Error", map[string]string{"id": "1", "comment_id": "comment_id"})
	helper(repoHelperErrGetItem, "400 Bad Request", map[string]string{"id": "1", "comment_id": "comment_id"})
	helper(repoHelperBadMarshal, "500 Internal Server Error", map[string]string{"id": "1", "comment_id": "comment_id"})
}

func TestMakeUpvoteAndDownvoteForPostByID(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperErrAddVote handlers.HandlerRepositoryInterface
	var repoHelperErrCalcScore handlers.HandlerRepositoryInterface
	var repoHelperErrGetItem handlers.HandlerRepositoryInterface
	var repoHelperBadMarshal handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperErrAddVote = &mocks.HandlerRepositoryInterface{}
	repoHelperErrCalcScore = &mocks.HandlerRepositoryInterface{}
	repoHelperErrGetItem = &mocks.HandlerRepositoryInterface{}
	repoHelperBadMarshal = &mocks.HandlerRepositoryInterface{}

	copyItem := getCopyTestItem()
	copyItem.Votes = []vote.Vote{
		vote.Vote{testItem.Author.UserName, 1},
	}

	repoHelper.(*mocks.HandlerRepositoryInterface).On("AddVoteByID", "1", mock.AnythingOfType("vote.Vote")).Return(nil)
	repoHelper.(*mocks.HandlerRepositoryInterface).On("CalcUpvotePercentAndScorePostByID", "1").Return(nil)
	repoHelper.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(copyItem, nil)
	repoHelperErrAddVote.(*mocks.HandlerRepositoryInterface).On("AddVoteByID", "1", mock.AnythingOfType("vote.Vote")).Return(MockError)
	repoHelperErrCalcScore.(*mocks.HandlerRepositoryInterface).On("AddVoteByID", "1", mock.AnythingOfType("vote.Vote")).Return(nil)
	repoHelperErrCalcScore.(*mocks.HandlerRepositoryInterface).On("CalcUpvotePercentAndScorePostByID", "1").Return(MockError)
	repoHelperErrGetItem.(*mocks.HandlerRepositoryInterface).On("AddVoteByID", "1", mock.AnythingOfType("vote.Vote")).Return(nil)
	repoHelperErrGetItem.(*mocks.HandlerRepositoryInterface).On("CalcUpvotePercentAndScorePostByID", "1").Return(nil)
	repoHelperErrGetItem.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(nil, MockError)
	repoHelperBadMarshal.(*mocks.HandlerRepositoryInterface).On("AddVoteByID", "1", mock.AnythingOfType("vote.Vote")).Return(nil)
	repoHelperBadMarshal.(*mocks.HandlerRepositoryInterface).On("CalcUpvotePercentAndScorePostByID", "1").Return(nil)
	repoHelperBadMarshal.(*mocks.HandlerRepositoryInterface).On("GetItemByID", "1").Return(nil, nil)

	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}
	req := httptest.NewRequest("GET", "/api/post/1/upvote", nil)
	vars := map[string]string{
		"id": "1",
	}
	req = mux.SetURLVars(req, vars)
	ctx := context.WithValue(req.Context(), session.SessionKey, sess)
	w := httptest.NewRecorder()

	service.MakeUpvoteForPostByID(w, req.WithContext(ctx))

	resp := w.Result()
	bodyResp, err := ioutil.ReadAll(resp.Body)
	expected, errJS := json.Marshal(copyItem)

	assert.NoError(t, err)
	assert.NoError(t, errJS)
	assert.Equal(t, resp.Status, "200 OK")
	assert.Equal(t, expected, bodyResp)

	//bad session

	req = httptest.NewRequest("GET", "/api/post/1/downvote", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()

	service.MakeDownvoteForPostByID(w, req)

	resp = w.Result()

	assert.Equal(t, resp.Status, "500 Internal Server Error")

	helper := func(repo handlers.HandlerRepositoryInterface, status string, vars map[string]string) {
		req = httptest.NewRequest("GET", "/api/post/1/upvote", nil)
		req = mux.SetURLVars(req, vars)
		ctx = context.WithValue(req.Context(), session.SessionKey, sess)
		w = httptest.NewRecorder()
		service.ItemsRepo = repo

		service.MakeUpvoteForPostByID(w, req.WithContext(ctx))

		resp = w.Result()

		assert.Equal(t, resp.Status, status)
	}

	helper(repoHelperErrAddVote, "500 Internal Server Error", map[string]string{"id": "1"})
	helper(repoHelperErrCalcScore, "500 Internal Server Error", map[string]string{"id": "1"})
	helper(repoHelperErrGetItem, "400 Bad Request", map[string]string{"id": "1"})
	helper(repoHelperBadMarshal, "500 Internal Server Error", map[string]string{"id": "1"})
	helper(repoHelper, "502 Bad Gateway", map[string]string{})
}

func TestDeletePostByPostID(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperErrDeleteItem handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperErrDeleteItem = &mocks.HandlerRepositoryInterface{}

	repoHelper.(*mocks.HandlerRepositoryInterface).On("DeleteItemByItemID", "1").Return(nil)
	repoHelperErrDeleteItem.(*mocks.HandlerRepositoryInterface).On("DeleteItemByItemID", "1").Return(MockError)

	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}
	req := httptest.NewRequest("DELETE", "/api/post/1", nil)
	vars := map[string]string{
		"id": "1",
	}
	req = mux.SetURLVars(req, vars)
	w := httptest.NewRecorder()

	service.DeletePostByPostID(w, req)

	resp := w.Result()
	expected := `{"message":"success"}`
	bodyResp, err := ioutil.ReadAll(resp.Body)

	assert.NoError(t, err)
	assert.Equal(t, resp.Status, "200 OK")
	assert.Equal(t, expected, string(bodyResp))

	helper := func(repo handlers.HandlerRepositoryInterface, status string, vars map[string]string) {
		req = httptest.NewRequest("DELETE", "/api/post/1", nil)
		req = mux.SetURLVars(req, vars)
		w = httptest.NewRecorder()
		service.ItemsRepo = repo

		service.DeletePostByPostID(w, req)

		resp = w.Result()

		assert.Equal(t, resp.Status, status)
	}

	helper(repoHelper, "502 Bad Gateway", map[string]string{})
	helper(repoHelperErrDeleteItem, "400 Bad Request", map[string]string{"id": "1"})
}

func TestAllPostsByUserName(t *testing.T) {
	var repoHelper handlers.HandlerRepositoryInterface
	var repoHelperErrMarshal handlers.HandlerRepositoryInterface

	repoHelper = &mocks.HandlerRepositoryInterface{}
	repoHelperErrMarshal = &mocks.HandlerRepositoryInterface{}

	repoHelper.(*mocks.HandlerRepositoryInterface).On("GetAllItemsByUserName", testItem.Author.UserName).Return([]*items.Item{testItem})
	repoHelperErrMarshal.(*mocks.HandlerRepositoryInterface).On("GetAllItemsByUserName", testItem.Author.UserName).Return(nil)

	copyItem := getCopyTestItem()
	service := &handlers.Handler{
		Tmpl:      template.Must(template.ParseGlob("../../template/index.html")),
		Logger:    zap.NewNop().Sugar(),
		ItemsRepo: repoHelper,
	}
	req := httptest.NewRequest("GET", "/api/user/qwerty12345678", nil)
	vars := map[string]string{
		"user_login": "qwerty12345678",
	}
	req = mux.SetURLVars(req, vars)
	w := httptest.NewRecorder()

	service.AllPostsByUserName(w, req)

	resp := w.Result()

	expected, errJS := json.Marshal([]*items.Item{copyItem})
	bodyResp, err := ioutil.ReadAll(resp.Body)

	assert.NoError(t, err)
	assert.NoError(t, errJS)
	assert.Equal(t, resp.Status, "200 OK")
	assert.Equal(t, expected, bodyResp)

	helper := func(repo handlers.HandlerRepositoryInterface, status string, vars map[string]string) {
		req = httptest.NewRequest("GET", "/api/user/qwerty12345678", nil)
		req = mux.SetURLVars(req, vars)
		w = httptest.NewRecorder()
		service.ItemsRepo = repo

		service.AllPostsByUserName(w, req)

		resp = w.Result()

		assert.Equal(t, resp.Status, status)
	}

	helper(repoHelper, "502 Bad Gateway", map[string]string{})
	helper(repoHelperErrMarshal, "500 Internal Server Error", map[string]string{"user_login": "qwerty12345678"})
}
