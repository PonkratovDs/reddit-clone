package session

import (
	"context"
	"errors"
	"redditclone/pkg/user"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var (
	ErrNoAuth              = errors.New("No session found")
	ErrInternalServerError = errors.New("Internal server error")
)

type KeyContext string

const (
	SessionKey KeyContext = "sessionKey"
)

type Session struct {
	ID       string
	UserID   string
	UserName string
}

func NewSession(hmacSampleSecret []byte, userID, userName string) (*Session, error) {
	uJWT := user.JWTUser{
		UserName: userName,
		ID:       userID,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": uJWT,
		"iat":  time.Now().Unix(),
		"exp":  time.Now().Add(7 * 24 * time.Hour).Unix(),
	})

	tokenString, err := token.SignedString(hmacSampleSecret)
	if err != nil {
		return nil, err
	}

	return &Session{
		ID:       tokenString,
		UserID:   userID,
		UserName: userName,
	}, nil
}

func SessionFromContext(ctx context.Context) (*Session, error) {
	//fmt.Println(ctx.Value(SessionKey))
	sess, ok := ctx.Value(SessionKey).(*Session)
	if !ok || sess == nil {
		return nil, ErrNoAuth
	}
	return sess, nil
}
