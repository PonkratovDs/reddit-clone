package session

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
)

var (
	ErrParsing         = errors.New("error when parsing structure from JWT")
	ErrValidationToken = errors.New("token validation error")
)

type SessionsManager struct {
	data             *sql.DB
	hmacSampleSecret []byte
}

func NewSessionsMem(db *sql.DB) *SessionsManager {
	return &SessionsManager{
		data:             db,
		hmacSampleSecret: []byte("it's a secret"),
	}
}

func (sm *SessionsManager) Create(w http.ResponseWriter, userID, userName string) (*Session, error) {
	sess, err := NewSession(sm.hmacSampleSecret, userID, userName)
	if err != nil {
		return nil, err
	}
	_, err = sm.data.Exec(
		"INSERT INTO sessions (`id`, `userid`, `username`) VALUES (?, ?, ?)",
		sess.ID,
		sess.UserID,
		sess.UserName,
	)
	if err != nil {
		return nil, fmt.Errorf("error while adding session to database, err: %w", err)
	}
	return sess, nil
}

func (sm *SessionsManager) GetSessionFromJWT(tokenString string) (*Session, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return sm.hmacSampleSecret, nil
	})
	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		u, ok := claims["user"].(map[string]interface{})
		if !ok {
			return nil, ErrParsing
		}
		userName, okUserName := u["username"].(string)
		id, okID := u["id"].(string)
		if !okUserName || !okID {
			return nil, ErrParsing
		}
		session := &Session{
			ID:       tokenString,
			UserID:   id,
			UserName: userName,
		}
		return session, nil
	}
	return nil, ErrValidationToken
}
