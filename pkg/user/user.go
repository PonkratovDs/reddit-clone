package user

import (
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"

	"github.com/twharmon/gouid"
	"golang.org/x/crypto/scrypt"
)

type UnmarshalUser struct {
	UserName string `json:"username"`
	Password string `json:"password"`
}

type JWTUser struct {
	UserName string `schema:"username,required" json:"username"`
	ID       string `schema:"id,required" json:"id"`
}

type User struct {
	UserName string `schema:"username,required" json:"username"`
	ID       string `schema:"id,required" json:"id"`
	password string `schema:"-" json:"-"`
}

type UserRepo struct {
	salt    []byte
	n       int
	data    *sql.DB
	CurUser *User
}

func NewUserRepo(db *sql.DB) *UserRepo {
	return &UserRepo{
		salt: []byte{0xc7, 0x28, 0xf2, 0x58, 0xa7, 0x6a, 0xad, 0x7b},
		n:    1 << 15,
		data: db,
	}
}

var (
	ErrNoUser              = errors.New("No user found")
	ErrBadPass             = errors.New("Invalid password")
	ErrInternalServerError = errors.New("Internal server error")
	ErrAlreadyExistUser    = errors.New("user with this login already exists")
	idForTest              = ""
)

func (repo *UserRepo) Authorize(username, pass string) (*User, error) {
	u := &User{}
	row := repo.data.QueryRow("SELECT username, id, password FROM users WHERE username = ?", username)
	err := row.Scan(&u.UserName, &u.ID, &u.password)
	if err != nil {
		return nil, fmt.Errorf("error while searching and retrieving a user from the database, error: %w", err)
	}
	dk, err := scrypt.Key([]byte(pass), repo.salt, repo.n, 8, 1, 32)
	if err != nil {
		return nil, ErrInternalServerError
	}
	if u.password != base64.StdEncoding.EncodeToString(dk) {
		return nil, ErrBadPass
	}
	repo.CurUser = u
	return u, nil
}

func (repo *UserRepo) AddUser(username, pass string) (string, error) {
	u := &User{}
	row := repo.data.QueryRow("SELECT username, id, password FROM users WHERE username = ?", username)
	err := row.Scan(&u.UserName, &u.ID, &u.password)
	if err != sql.ErrNoRows && u.UserName != "" {
		return "", ErrAlreadyExistUser
	} else if err != nil && err != sql.ErrNoRows {
		return "", fmt.Errorf("error while searching and retrieving a user from the database, error: %w", err)
	}

	dk, err := scrypt.Key([]byte(pass), repo.salt, repo.n, 8, 1, 32)
	if err != nil {
		return "", ErrInternalServerError
	}
	id := gouid.String(24)
	if idForTest != "" {
		id = "1"
	}
	nU := &User{
		ID:       id,
		password: base64.StdEncoding.EncodeToString(dk),
		UserName: username,
	}
	_, err = repo.data.Exec(
		"INSERT INTO users (`username`, `id`, `password`) VALUES (?, ?, ?)",
		nU.UserName,
		nU.ID,
		nU.password,
	)
	if err != nil {
		return "", fmt.Errorf("error while writing the user to the database, error: %w", err)
	}
	repo.CurUser = nU
	return id, nil
}
