package user

import (
	"database/sql"
	"fmt"
	"reflect"
	"testing"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestAuthorize(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("cant create mock: %s", err)
	}
	defer db.Close()

	rows := sqlmock.NewRows([]string{"username", "id", "password"})
	expect := []*User{
		&User{"username", "", "LhomnupIuy2FaA/hz0+wC/CJatjXVTX9TX1W671spUQ="},
	}
	for _, user := range expect {
		rows = rows.AddRow(user.UserName, user.ID, user.password)
	}

	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE").
		WithArgs("username").
		WillReturnRows(rows)

	repo := NewUserRepo(db)
	u, err := repo.Authorize("username", "passpass")
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if !reflect.DeepEqual(u, expect[0]) {
		t.Errorf("results not match, want %v, have %v", expect[0], u)
		return
	}

	//bad pass

	rows = sqlmock.NewRows([]string{"username", "id", "password"}).
		AddRow("username", "", "bad_pass")

	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE").
		WithArgs("username").
		WillReturnRows(rows)

	_, err = repo.Authorize("username", "passpass")

	if err != ErrBadPass {
		t.Errorf("the expected error %s was not received. It came: %s", ErrBadPass, err)
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}

	//error scrypt.Key
	repo.n = 1

	rows = sqlmock.NewRows([]string{"username", "id", "password"}).
		AddRow("username", "", "LhomnupIuy2FaA/hz0+wC/CJatjXVTX9TX1W671spUQ=")

	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE").
		WithArgs("username").
		WillReturnRows(rows)

	_, err = repo.Authorize("username", "passpass")
	if err != ErrInternalServerError {
		t.Errorf("the expected error %s was not received. It came: %s", ErrInternalServerError, err)
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}

	repo.n = 1 << 15

	//query error
	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE").
		WithArgs("username_1").
		WillReturnError(fmt.Errorf("db_error"))

	_, err = repo.Authorize("username_1", "passpass")

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}
}

func TestAddUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("cant create mock: %s", err)
	}
	defer db.Close()

	repo := NewUserRepo(db)

	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE username").
		WithArgs("username").
		WillReturnError(sql.ErrNoRows)

	idForTest = "1"
	mock.
		ExpectExec("INSERT INTO users").
		WithArgs("username", "1", "LhomnupIuy2FaA/hz0+wC/CJatjXVTX9TX1W671spUQ=").
		WillReturnResult(sqlmock.NewResult(1, 1))

	id, err := repo.AddUser("username", "passpass")
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if id != "1" {
		t.Errorf("wrong expected id. Expected %s: Received: %s", "1", id)
	}

	//error scrypt.Key
	repo.n = 1

	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE username").
		WithArgs("username").
		WillReturnError(sql.ErrNoRows)

	_, err = repo.AddUser("username", "passpass")

	if err != ErrInternalServerError {
		t.Errorf("the expected error %s was not received. It came: %s", ErrInternalServerError, err)
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}

	repo.n = 1 << 15

	// err already exist user
	rows := sqlmock.NewRows([]string{"username", "id", "password"}).
		AddRow("username", "1", "LhomnupIuy2FaA/hz0+wC/CJatjXVTX9TX1W671spUQ=")
	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE username").
		WithArgs("username").
		WillReturnRows(rows)

	_, err = repo.AddUser("username", "passpass")

	if err != ErrAlreadyExistUser {
		t.Errorf("the expected error %s was not received. It came: %s", ErrAlreadyExistUser, err)
		return
	}
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}

	//err query row
	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE username").
		WithArgs("username").
		WillReturnError(fmt.Errorf("db_error"))

	_, err = repo.AddUser("username", "passpass")

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}

	//err exec
	mock.
		ExpectQuery("SELECT username, id, password FROM users WHERE username").
		WithArgs("username").
		WillReturnError(sql.ErrNoRows)

	mock.
		ExpectExec("INSERT INTO users").
		WithArgs("username", "1", "LhomnupIuy2FaA/hz0+wC/CJatjXVTX9TX1W671spUQ=").
		WillReturnError(fmt.Errorf("db_error"))

	_, err = repo.AddUser("username", "passpass")

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
		return
	}
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}

	idForTest = ""

}
