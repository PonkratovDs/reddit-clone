package items

import (
	"redditclone/pkg/comment"
	"redditclone/pkg/user"
	"redditclone/pkg/vote"

	"gopkg.in/mgo.v2/bson"
)

type Item struct {
	Author           user.User         `schema:"author,required" json:"author"`
	Category         string            `schema:"category,required" json:"category"`
	Comments         []comment.Comment `schema:"comments,required" json:"comments"`
	Created          string            `schema:"created,required" json:"created"`
	ItemID           bson.ObjectId     `schema:"id,required" json:"id" bson:"_id"`
	Score            int               `schema:"score,required" json:"score"`
	Title            string            `schema:"title,required" json:"title"`
	Text             string            `schema:"text" json:"text,omitempty"`
	Type             string            `schema:"type,required" json:"type"`
	UpVotePercentage int               `schema:"upvotePercentage,required" json:"upvotePercentage"`
	URL              string            `schema:"text" json:"url,omitempty"`
	Views            int               `schema:"views,required" json:"views"`
	Votes            []vote.Vote       `schema:"votes,required" json:"votes"`
}

type UnmarshalItem struct {
	Category string `json:"category"`
	Type     string `json:"type"`
	Title    string `json:"title"`
	Text     string `json:"text,omitempty"`
	URL      string `json:"url,omitempty"`
}
