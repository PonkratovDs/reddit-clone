package items_test

import (
	"errors"
	"fmt"
	"redditclone/pkg/comment"
	"redditclone/pkg/items"
	"redditclone/pkg/items/mocks"
	"redditclone/pkg/user"
	"redditclone/pkg/vote"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func TestGetItemByID(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var collectionHelper items.MyCollection
	var queryHelper items.MyQuery
	var queryHelperErr items.MyQuery

	dbHelper = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	queryHelper = &mocks.MyQuery{}
	queryHelperErr = &mocks.MyQuery{}
	errBsonID := bson.NewObjectId()

	queryHelper.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Category = "mocked-item"
	})
	queryHelperErr.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(errors.New("mocked-error"))
	collectionHelper.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelper)
	collectionHelper.(*mocks.MyCollection).On("Find", bson.M{"_id": errBsonID}).Return(queryHelperErr)
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)

	repo := items.NewItemsRepo(dbHelper)

	item, err := repo.GetItemByID(items.TestID.Hex())

	assert.NoError(t, err)
	assert.Equal(t, "mocked-item", item.Category)

	_, err = repo.GetItemByID(errBsonID.Hex())

	assert.EqualError(t, err, "no item found, error: mocked-error")

	_, err = repo.GetItemByID(errBsonID.String())
	assert.EqualError(t, err, "no valid id")
}

func TestAddItem(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var dbHelperErr items.DatabaseHelper
	var collectionHelper items.MyCollection
	var collectionHelperErr items.MyCollection

	dbHelper = &mocks.DatabaseHelper{}
	dbHelperErr = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	collectionHelperErr = &mocks.MyCollection{}

	collectionHelper.(*mocks.MyCollection).On("Insert", mock.AnythingOfType("bson.M")).Return(nil)
	collectionHelperErr.(*mocks.MyCollection).On("Insert", mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)
	dbHelperErr.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErr)

	repo := items.NewItemsRepo(dbHelper)

	err := repo.AddItem(&items.Testitem)
	assert.NoError(t, err)

	repo = items.NewItemsRepo(dbHelperErr)

	err = repo.AddItem(&items.Testitem)
	assert.EqualError(t, err, "error when adding a post to the database, error: mocked-error")
}

func TestUpdateItem(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var dbHelperErrNotFound items.DatabaseHelper
	var dbHelperErr items.DatabaseHelper
	var collectionHelper items.MyCollection
	var collectionHelperErrNotFound items.MyCollection
	var collectionHelperErr items.MyCollection

	dbHelper = &mocks.DatabaseHelper{}
	dbHelperErrNotFound = &mocks.DatabaseHelper{}
	dbHelperErr = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	collectionHelperErrNotFound = &mocks.MyCollection{}
	collectionHelperErr = &mocks.MyCollection{}

	collectionHelper.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(nil)
	collectionHelperErrNotFound.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(mgo.ErrNotFound)
	collectionHelperErr.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)
	dbHelperErrNotFound.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErrNotFound)
	dbHelperErr.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErr)

	repo := items.NewItemsRepo(dbHelper)
	repoErrNotFound := items.NewItemsRepo(dbHelperErrNotFound)
	repoErr := items.NewItemsRepo(dbHelperErr)

	err := repo.UpdateItem(&items.Testitem)
	assert.NoError(t, err)

	err = repoErrNotFound.UpdateItem(&items.Testitem)
	assert.EqualError(t, err, `item with this ID ObjectIdHex("") does not exist, error: not found`)

	err = repoErr.UpdateItem(&items.Testitem)
	assert.EqualError(t, err, `error when updating the item to the database, error: mocked-error`)
}

func TestGetAllItemsAndGetAllItemsByCategory(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var dbHelperCategory items.DatabaseHelper
	var collectionHelper items.MyCollection
	var collectionHelperCategory items.MyCollection
	var queryHelper items.MyQuery

	dbHelper = &mocks.DatabaseHelper{}
	dbHelperCategory = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	collectionHelperCategory = &mocks.MyCollection{}
	queryHelper = &mocks.MyQuery{}

	queryHelper.(*mocks.MyQuery).On("All", mock.AnythingOfType("*[]*items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(*[]*items.Item)
		item := &items.Item{Category: "mock"}
		*arg = append(*arg, item)
	})
	collectionHelper.(*mocks.MyCollection).On("Find", bson.M{}).Return(queryHelper)
	collectionHelperCategory.(*mocks.MyCollection).On("Find", bson.M{"category": "mock"}).Return(queryHelper)
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)
	dbHelperCategory.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperCategory)

	repo := items.NewItemsRepo(dbHelper)

	result := repo.GetAllItems()

	assert.Equal(t, "mock", (*result[0]).Category)

	repo = items.NewItemsRepo(dbHelperCategory)

	result = repo.GetAllItemsByCategory("mock")
	assert.Equal(t, "mock", (*result[0]).Category)
}

func TestAddCommentsByID(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var dbHelperErr items.DatabaseHelper
	var dbHelperErrUpdate items.DatabaseHelper
	var collectionHelper items.MyCollection
	var collectionHelperErr items.MyCollection
	var collectionHelperErrUpdate items.MyCollection
	var queryHelper items.MyQuery
	var queryHelperErr items.MyQuery

	var comments []comment.Comment

	dbHelper = &mocks.DatabaseHelper{}
	dbHelperErr = &mocks.DatabaseHelper{}
	dbHelperErrUpdate = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	collectionHelperErr = &mocks.MyCollection{}
	collectionHelperErrUpdate = &mocks.MyCollection{}
	queryHelper = &mocks.MyQuery{}
	queryHelperErr = &mocks.MyQuery{}

	queryHelper.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Comments = []comment.Comment{comment.Comment{Body: "some_comment"}}
	})
	collectionHelper.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelper)
	collectionHelper.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(nil).Run(func(args mock.Arguments) {
		arg1 := args.Get(1).(bson.M)
		comments = append(comments, arg1["comments"].([]comment.Comment)...)
	})
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)

	repo := items.NewItemsRepo(dbHelper)

	err := repo.AddCommentsByID(items.TestID.Hex(), comment.Comment{Body: "new_comment"})
	assert.NoError(t, err)
	assert.Equal(t, "some_comment", comments[0].Body)
	assert.Equal(t, "new_comment", comments[1].Body)

	queryHelperErr.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(errors.New("mocked-error"))
	collectionHelperErr.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperErr)
	dbHelperErr.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErr)

	repo = items.NewItemsRepo(dbHelperErr)

	err = repo.AddCommentsByID(items.TestID.Hex(), comment.Comment{})
	assert.EqualError(t, err, "no item found, error: mocked-error")

	collectionHelperErrUpdate.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelper)
	collectionHelperErrUpdate.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	dbHelperErrUpdate.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErrUpdate)

	repo = items.NewItemsRepo(dbHelperErrUpdate)

	err = repo.AddCommentsByID(items.TestID.Hex(), comment.Comment{})
	assert.EqualError(t, err, "error when updating the item to the database, error: mocked-error")
}

func TestDeleteCommentByPostAndCommentID(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var dbHelperErr items.DatabaseHelper
	var dbHelperNoComments items.DatabaseHelper
	var dbHelperErrUpdate items.DatabaseHelper
	var collectionHelper items.MyCollection
	var collectionHelperErr items.MyCollection
	var collectionHelperNoComments items.MyCollection
	var collectionHelperErrUpdate items.MyCollection
	var queryHelperNoComments items.MyQuery
	var queryHelperErr items.MyQuery
	var queryHelper items.MyQuery
	var queryHelperErrUpdate items.MyQuery

	var comments []comment.Comment

	dbHelperNoComments = &mocks.DatabaseHelper{}
	dbHelperErr = &mocks.DatabaseHelper{}
	dbHelper = &mocks.DatabaseHelper{}
	dbHelperErrUpdate = &mocks.DatabaseHelper{}
	collectionHelperNoComments = &mocks.MyCollection{}
	collectionHelper = &mocks.MyCollection{}
	collectionHelperErr = &mocks.MyCollection{}
	collectionHelperErrUpdate = &mocks.MyCollection{}
	queryHelperNoComments = &mocks.MyQuery{}
	queryHelper = &mocks.MyQuery{}
	queryHelperErr = &mocks.MyQuery{}
	queryHelperErrUpdate = &mocks.MyQuery{}

	queryHelperNoComments.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil)
	collectionHelperNoComments.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperNoComments)
	dbHelperNoComments.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperNoComments)

	repo := items.NewItemsRepo(dbHelperNoComments)

	err := repo.DeleteCommentByPostAndCommentID(items.TestID.Hex(), "comment_id")
	assert.EqualError(t, err, items.ErrNoComment.Error())

	queryHelper.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Comments = []comment.Comment{
			comment.Comment{ID: "1"},
			comment.Comment{ID: "comment_id"},
			comment.Comment{ID: "2"},
		}
	})
	collectionHelper.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelper)
	collectionHelper.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(nil).Run(func(args mock.Arguments) {
		arg1 := args.Get(1).(bson.M)
		comments = append(comments, arg1["comments"].([]comment.Comment)...)
	})
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)

	repo = items.NewItemsRepo(dbHelper)

	err = repo.DeleteCommentByPostAndCommentID(items.TestID.Hex(), "comment_id")
	assert.NoError(t, err)
	assert.Equal(t, "1", comments[0].ID)
	assert.Equal(t, "2", comments[1].ID)

	comments = []comment.Comment{}

	queryHelperErr.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(errors.New("mocked-error"))
	collectionHelperErr.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperErr)
	dbHelperErr.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErr)

	repo = items.NewItemsRepo(dbHelperErr)

	err = repo.DeleteCommentByPostAndCommentID(items.TestID.Hex(), "comment_id")
	assert.EqualError(t, err, "no item found, error: mocked-error")

	queryHelperErrUpdate.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Comments = []comment.Comment{
			comment.Comment{ID: "1"},
			comment.Comment{ID: "comment_id"},
			comment.Comment{ID: "2"},
		}
	})
	collectionHelperErrUpdate.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperErrUpdate)
	collectionHelperErrUpdate.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	dbHelperErrUpdate.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErrUpdate)

	repo = items.NewItemsRepo(dbHelperErrUpdate)

	err = repo.DeleteCommentByPostAndCommentID(items.TestID.Hex(), "comment_id")
	assert.EqualError(t, err, "error when updating the item to the database, error: mocked-error")
}

func TestCalcUpvotePercentAndScorePostByID(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var dbHelper0Score items.DatabaseHelper
	var dbHelper0ScoreErr items.DatabaseHelper
	var dbHelperErr items.DatabaseHelper
	var dbHelperErrUpdate items.DatabaseHelper
	var collectionHelper items.MyCollection
	var collectionHelper0Score items.MyCollection
	var collectionHelper0ScoreErr items.MyCollection
	var collectionHelperErr items.MyCollection
	var collectionHelperErrUpdate items.MyCollection
	var queryHelper items.MyQuery
	var queryHelper0Score items.MyQuery
	var queryHelper0ScoreErr items.MyQuery
	var queryHelperErr items.MyQuery
	var queryHelperErrUpdate items.MyQuery

	var upVotePercentage int
	var score int

	dbHelperErr = &mocks.DatabaseHelper{}
	dbHelper = &mocks.DatabaseHelper{}
	dbHelper0Score = &mocks.DatabaseHelper{}
	dbHelper0ScoreErr = &mocks.DatabaseHelper{}
	dbHelperErrUpdate = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	collectionHelper0Score = &mocks.MyCollection{}
	collectionHelper0ScoreErr = &mocks.MyCollection{}
	collectionHelperErr = &mocks.MyCollection{}
	collectionHelperErrUpdate = &mocks.MyCollection{}
	queryHelper = &mocks.MyQuery{}
	queryHelper0Score = &mocks.MyQuery{}
	queryHelper0ScoreErr = &mocks.MyQuery{}
	queryHelperErr = &mocks.MyQuery{}
	queryHelperErrUpdate = &mocks.MyQuery{}

	queryHelper.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Votes = []vote.Vote{
			vote.Vote{Vote: 1},
			vote.Vote{Vote: 1},
			vote.Vote{Vote: -1},
		}
	})
	collectionHelper.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelper)
	collectionHelper.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(nil).Run(func(args mock.Arguments) {
		arg1 := args.Get(1).(bson.M)
		upVotePercentage, score = arg1["upvotePercentage"].(int), arg1["score"].(int)
	})
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)

	repo := items.NewItemsRepo(dbHelper)

	err := repo.CalcUpvotePercentAndScorePostByID(items.TestID.Hex())
	assert.NoError(t, err)
	assert.Equal(t, 66, upVotePercentage)
	assert.Equal(t, 1, score)

	queryHelper0Score.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Votes = []vote.Vote{
			vote.Vote{Vote: 1},
			vote.Vote{Vote: -1},
		}
	})
	collectionHelper0Score.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelper0Score)
	collectionHelper0Score.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(nil).Run(func(args mock.Arguments) {
		arg1 := args.Get(1).(bson.M)
		upVotePercentage, score = arg1["upvotePercentage"].(int), arg1["score"].(int)
	})
	dbHelper0Score.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper0Score)

	repo = items.NewItemsRepo(dbHelper0Score)

	err = repo.CalcUpvotePercentAndScorePostByID(items.TestID.Hex())
	assert.NoError(t, err)
	assert.Equal(t, 0, upVotePercentage)
	assert.Equal(t, 0, score)

	queryHelper0ScoreErr.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Votes = []vote.Vote{
			vote.Vote{Vote: 1},
			vote.Vote{Vote: -1},
		}
	})
	collectionHelper0ScoreErr.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelper0ScoreErr)
	collectionHelper0ScoreErr.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	dbHelper0ScoreErr.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper0ScoreErr)

	repo = items.NewItemsRepo(dbHelper0ScoreErr)

	err = repo.CalcUpvotePercentAndScorePostByID(items.TestID.Hex())
	assert.EqualError(t, err, "error when updating the item to the database, error: mocked-error")

	queryHelperErr.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(errors.New("mocked-error"))
	collectionHelperErr.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperErr)
	dbHelperErr.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErr)

	repo = items.NewItemsRepo(dbHelperErr)

	err = repo.CalcUpvotePercentAndScorePostByID(items.TestID.Hex())
	assert.EqualError(t, err, "no item found, error: mocked-error")

	queryHelperErrUpdate.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Votes = []vote.Vote{
			vote.Vote{Vote: 1},
			vote.Vote{Vote: 1},
			vote.Vote{Vote: -1},
		}
	})
	collectionHelperErrUpdate.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperErrUpdate)
	collectionHelperErrUpdate.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	dbHelperErrUpdate.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErrUpdate)

	repo = items.NewItemsRepo(dbHelperErrUpdate)

	err = repo.CalcUpvotePercentAndScorePostByID(items.TestID.Hex())
	assert.EqualError(t, err, "error when updating the item to the database, error: mocked-error")
}

func TestAddVoteByID(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var dbHelperWAdd items.DatabaseHelper
	var dbHelperWAddErrUpdate items.DatabaseHelper
	var dbHelperErr items.DatabaseHelper
	var dbHelperErrUpdate items.DatabaseHelper
	var collectionHelper items.MyCollection
	var collectionHelperWAdd items.MyCollection
	var collectionHelperWAddErrUpdate items.MyCollection
	var collectionHelperErr items.MyCollection
	var collectionHelperErrUpdate items.MyCollection
	var queryHelper items.MyQuery
	var queryHelperWAdd items.MyQuery
	var queryHelperWAddErrUpdate items.MyQuery
	var queryHelperErr items.MyQuery
	var queryHelperErrUpdate items.MyQuery

	var votes []vote.Vote

	dbHelper = &mocks.DatabaseHelper{}
	dbHelperWAdd = &mocks.DatabaseHelper{}
	dbHelperErr = &mocks.DatabaseHelper{}
	dbHelperErrUpdate = &mocks.DatabaseHelper{}
	dbHelperWAddErrUpdate = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	collectionHelperWAdd = &mocks.MyCollection{}
	collectionHelperErr = &mocks.MyCollection{}
	collectionHelperErrUpdate = &mocks.MyCollection{}
	collectionHelperWAddErrUpdate = &mocks.MyCollection{}
	queryHelper = &mocks.MyQuery{}
	queryHelperWAdd = &mocks.MyQuery{}
	queryHelperErr = &mocks.MyQuery{}
	queryHelperErrUpdate = &mocks.MyQuery{}
	queryHelperWAddErrUpdate = &mocks.MyQuery{}

	queryHelper.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Votes = []vote.Vote{
			vote.Vote{Vote: 1, UserID: "1"},
			vote.Vote{Vote: 1, UserID: "2"},
		}
	})
	collectionHelper.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelper)
	collectionHelper.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(nil).Run(func(args mock.Arguments) {
		arg1 := args.Get(1).(bson.M)
		votes = append(votes, arg1["votes"].([]vote.Vote)...)
	})
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)

	repo := items.NewItemsRepo(dbHelper)

	err := repo.AddVoteByID(items.TestID.Hex(), vote.Vote{Vote: -1, UserID: "id"})

	assert.NoError(t, err)
	assert.EqualValues(t, []vote.Vote{
		vote.Vote{Vote: 1, UserID: "1"},
		vote.Vote{Vote: 1, UserID: "2"},
		vote.Vote{Vote: -1, UserID: "id"},
	}, votes)

	votes = []vote.Vote{}

	queryHelperWAdd.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Votes = []vote.Vote{
			vote.Vote{Vote: 1, UserID: "1"},
			vote.Vote{Vote: 1, UserID: "id"},
		}
	})
	collectionHelperWAdd.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperWAdd)
	collectionHelperWAdd.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(nil).Run(func(args mock.Arguments) {
		arg1 := args.Get(1).(bson.M)
		votes = append(votes, arg1["votes"].([]vote.Vote)...)
	})
	dbHelperWAdd.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperWAdd)

	repo = items.NewItemsRepo(dbHelperWAdd)

	err = repo.AddVoteByID(items.TestID.Hex(), vote.Vote{Vote: -1, UserID: "id"})

	assert.NoError(t, err)
	assert.EqualValues(t, []vote.Vote{
		vote.Vote{Vote: 1, UserID: "1"},
		vote.Vote{Vote: -1, UserID: "id"},
	}, votes)

	queryHelperErr.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(errors.New("mocked-error"))
	collectionHelperErr.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperErr)
	dbHelperErr.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErr)

	repo = items.NewItemsRepo(dbHelperErr)

	err = repo.AddVoteByID(items.TestID.Hex(), vote.Vote{Vote: -1, UserID: "id"})

	assert.EqualError(t, err, "no item found, error: mocked-error")

	queryHelperErrUpdate.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Votes = []vote.Vote{
			vote.Vote{Vote: 1, UserID: "1"},
			vote.Vote{Vote: 1, UserID: "2"},
		}
	})
	collectionHelperErrUpdate.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperErrUpdate)
	collectionHelperErrUpdate.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	dbHelperErrUpdate.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErrUpdate)

	repo = items.NewItemsRepo(dbHelperErrUpdate)

	err = repo.AddVoteByID(items.TestID.Hex(), vote.Vote{Vote: -1, UserID: "id"})

	assert.EqualError(t, err, "error when updating the item to the database, error: mocked-error")

	queryHelperWAddErrUpdate.(*mocks.MyQuery).On("One", mock.AnythingOfType("**items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(**items.Item)
		(*arg).Votes = []vote.Vote{
			vote.Vote{Vote: 1, UserID: "1"},
			vote.Vote{Vote: 1, UserID: "id"},
		}
	})
	collectionHelperWAddErrUpdate.(*mocks.MyCollection).On("Find", bson.M{"_id": items.TestID}).Return(queryHelperWAddErrUpdate)
	collectionHelperWAddErrUpdate.(*mocks.MyCollection).On("Update", mock.AnythingOfType("bson.M"), mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	dbHelperWAddErrUpdate.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperWAddErrUpdate)

	repo = items.NewItemsRepo(dbHelperWAddErrUpdate)

	err = repo.AddVoteByID(items.TestID.Hex(), vote.Vote{Vote: -1, UserID: "id"})

	assert.EqualError(t, err, "error when updating the item to the database, error: mocked-error")
}

func TestDeleteItemByItemID(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var dbHelperErrRemove items.DatabaseHelper
	var dbHelperErrRemoveNotFound items.DatabaseHelper
	var collectionHelper items.MyCollection
	var collectionHelperErrRemove items.MyCollection
	var collectionHelperErrRemoveNotFound items.MyCollection

	dbHelper = &mocks.DatabaseHelper{}
	dbHelperErrRemove = &mocks.DatabaseHelper{}
	dbHelperErrRemoveNotFound = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	collectionHelperErrRemove = &mocks.MyCollection{}
	collectionHelperErrRemoveNotFound = &mocks.MyCollection{}

	collectionHelper.(*mocks.MyCollection).On("Remove", mock.AnythingOfType("bson.M")).Return(nil)
	collectionHelperErrRemove.(*mocks.MyCollection).On("Remove", mock.AnythingOfType("bson.M")).Return(errors.New("mocked-error"))
	collectionHelperErrRemoveNotFound.(*mocks.MyCollection).On("Remove", mock.AnythingOfType("bson.M")).Return(mgo.ErrNotFound)
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)
	dbHelperErrRemove.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErrRemove)
	dbHelperErrRemoveNotFound.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelperErrRemoveNotFound)

	repo := items.NewItemsRepo(dbHelper)

	err := repo.DeleteItemByItemID(items.TestID.Hex())

	assert.NoError(t, err)

	helper := func(repo *items.ItemsRepo, errorStr string, id string) {
		err := repo.DeleteItemByItemID(id)
		assert.EqualError(t, err, errorStr)
	}

	helper(items.NewItemsRepo(dbHelperErrRemove), "error when updating the item to the database, error: mocked-error", items.TestID.Hex())
	helper(items.NewItemsRepo(dbHelperErrRemoveNotFound), fmt.Sprintf("item with this ID %s does not exist, error: not found", items.TestID.Hex()), items.TestID.Hex())
	helper(items.NewItemsRepo(dbHelper), items.ErrNoValidID.Error(), items.TestID.String())
}

func TestGetAllItemsByUserName(t *testing.T) {
	var dbHelper items.DatabaseHelper
	var collectionHelper items.MyCollection
	var queryHelper items.MyQuery

	dbHelper = &mocks.DatabaseHelper{}
	collectionHelper = &mocks.MyCollection{}
	queryHelper = &mocks.MyQuery{}

	queryHelper.(*mocks.MyQuery).On("All", mock.AnythingOfType("*[]*items.Item")).Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(*[]*items.Item)
		*arg = []*items.Item{
			&items.Item{
				Author: user.User{UserName: "vasya"},
			},
			&items.Item{
				Text:   "text_1",
				Author: user.User{UserName: "sanya"},
			},
			&items.Item{
				Text:   "text_2",
				Author: user.User{UserName: "sanya"},
			},
		}
	})
	collectionHelper.(*mocks.MyCollection).On("Find", bson.M{}).Return(queryHelper)
	dbHelper.(*mocks.DatabaseHelper).On("Collection", "items").Return(collectionHelper)

	repo := items.NewItemsRepo(dbHelper)

	itemsUser := repo.GetAllItemsByUserName("sanya")

	assert.EqualValues(t, []*items.Item{
		&items.Item{
			Text:   "text_1",
			Author: user.User{UserName: "sanya"},
		},
		&items.Item{
			Text:   "text_2",
			Author: user.User{UserName: "sanya"},
		},
	}, itemsUser)
}
