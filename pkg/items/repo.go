package items

import (
	"errors"
	"fmt"
	"redditclone/pkg/comment"
	"redditclone/pkg/vote"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	ErrNoItem       = errors.New("no item found")
	ErrTypeCategory = errors.New("the category for this post does not match")
	ErrNoComment    = errors.New("no comment found")
	ErrNoValidID    = errors.New("no valid id")
	Testitem        = Item{}
	TestID          = bson.NewObjectId()
)

const collectionName = "items"

type ItemsRepo struct {
	data DatabaseHelper
}

//go:generate mockgen -source=repo.go -destination=repo_mock.go -package=items MyCollection MyQuery DatabaseHelper
type MyCollection interface {
	Insert(docs interface{}) error
	Find(query interface{}) MyQuery
	Update(selector interface{}, update interface{}) error
	Remove(selector interface{}) error
}

type MyQuery interface {
	All(result interface{}) error
	One(result interface{}) (err error)
}

/*
item: &items.Item{Author:user.User{UserName:"pass", ID:"k5oqviyvef3ryu10ck2c04no", password:""}, Category:"music", Comments:[]comment.Comment{}, Created:"2020-11-20T12:06:04+03:00", ItemID:"", Score:1, Title:"test", Text:"test", Type:"text", UpVotePercentage:100, URL:"", Views:0, Votes:[]vote.Vote{vote.Vote{UserID:"k5oqviyvef3ryu10ck2c04no", Vote:1}}} , bsonItem: bson.M{"author":user.User{UserName:"pass", ID:"k5oqviyvef3ryu10ck2c04no", password:""}, "category":"music", "comments":[]comment.Comment{}, "created":"2020-11-20T12:06:04+03:00", "id":"_\xb7\x86\xfc\x17}\xabn/\x8a\x81F", "score":1, "text":"test", "title":"test", "type":"text", "upvotePercentage":100, "url":"", "views":0, "votes":[]vote.Vote{vote.Vote{UserID:"k5oqviyvef3ryu10ck2c04no", Vote:1}}}
id: 5fb786fcecc8c4b2274a0d7e, idB: ObjectIdHex("5fb786fcecc8c4b2274a0d7e"), query: &mgo.Query{m:sync.Mutex{state:0, sema:0x0}, session:(*mgo.Session)(0xc0000a8000), query:mgo.query{op:mgo.queryOp{collection:"coursera.items", query:bson.M{"_id":"_\xb7\x86\xfc\xec\xc8Ĳ'J\r~"}, skip:0, limit:0, selector:interface {}(nil), flags:0x0, replyFunc:(mgo.replyFunc)(nil), mode:0, options:mgo.queryWrapper{Query:interface {}(nil), OrderBy:interface {}(nil), Hint:interface {}(nil), Explain:false, Snapshot:false, ReadPreference:bson.D(nil), MaxScan:0, MaxTimeMS:0, Comment:""}, hasOptions:false, serverTags:[]bson.D(nil)}, prefetch:0.25, limit:0}}{"level":"info","ts":1605863168.9210331,"caller":"middleware/accesslog.go:18","msg":"New request","method":"GET","remote_addr":"[::1]:55556","url":"/api/post/5fb786fcecc8c4b2274a0d7e","time":0.008514071}
*/

func NewItemsRepo(db DatabaseHelper) *ItemsRepo {
	return &ItemsRepo{
		data: db,
	}
}

func (ir *ItemsRepo) AddItem(item *Item) error {
	newItem := bson.M{
		"id":               bson.NewObjectId(),
		"author":           item.Author,
		"category":         item.Category,
		"comments":         item.Comments,
		"created":          item.Created,
		"score":            item.Score,
		"title":            item.Title,
		"text":             item.Text,
		"type":             item.Type,
		"upvotePercentage": item.UpVotePercentage,
		"url":              item.URL,
		"views":            item.Views,
		"votes":            item.Votes,
	}
	err := ir.data.Collection(collectionName).Insert(newItem)
	if err != nil {
		return fmt.Errorf("error when adding a post to the database, error: %w", err)
	}
	return nil
}

func (ir *ItemsRepo) GetItemByID(id string) (*Item, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, ErrNoValidID
	}
	idB := bson.ObjectIdHex(id)
	item := &Item{}
	err := ir.data.Collection(collectionName).Find(bson.M{"_id": idB}).One(&item)
	if err != nil {
		return nil, fmt.Errorf("%s, error: %w", ErrNoItem.Error(), err)
	}
	return item, nil
}

func (ir *ItemsRepo) UpdateItem(item *Item) error {
	err := ir.data.Collection(collectionName).Update(
		bson.M{"_id": item.ItemID},
		bson.M{
			"_id":              item.ItemID,
			"author":           item.Author,
			"category":         item.Category,
			"comments":         item.Comments,
			"created":          item.Created,
			"score":            item.Score,
			"title":            item.Title,
			"text":             item.Text,
			"type":             item.Type,
			"upvotePercentage": item.UpVotePercentage,
			"url":              item.URL,
			"views":            item.Views,
			"votes":            item.Votes,
		},
	)
	if err == mgo.ErrNotFound {
		return fmt.Errorf("item with this ID %s does not exist, error: %w", item.ItemID.String(), err)
	} else if err != nil {
		return fmt.Errorf("error when updating the item to the database, error: %w", err)
	}
	return nil
}

func (ir *ItemsRepo) GetAllItems() []*Item {
	items := []*Item{}
	ir.data.Collection(collectionName).Find(bson.M{}).All(&items)
	return items
}

func (ir *ItemsRepo) GetAllItemsByCategory(category string) []*Item {
	items := []*Item{}
	ir.data.Collection(collectionName).Find(bson.M{"category": category}).All(&items)
	return items
}

func (ir *ItemsRepo) AddCommentsByID(id string, comment comment.Comment) error {
	item, err := ir.GetItemByID(id)
	if err != nil {
		return err
	}
	item.Comments = append(item.Comments, comment)
	err = ir.UpdateItem(item)
	if err != nil {
		return err
	}
	return nil
}

func (ir *ItemsRepo) DeleteCommentByPostAndCommentID(id, commentID string) error {
	item, err := ir.GetItemByID(id)
	if err != nil {
		return err
	}
	newComments := []comment.Comment{}
	for idx, comment := range item.Comments {
		if comment.ID == commentID {
			newComments = append(newComments, item.Comments[:idx]...)
			newComments = append(newComments, item.Comments[idx+1:]...)
			item.Comments = newComments
			err = ir.UpdateItem(item)
			if err != nil {
				return err
			}
			return nil
		}
	}
	return ErrNoComment
}

func (ir *ItemsRepo) CalcUpvotePercentAndScorePostByID(id string) error {
	item, err := ir.GetItemByID(id)
	if err != nil {
		return err
	}
	cntUpvote, cntDownvote := 0, 0
	for _, vote := range item.Votes {
		if vote.Vote == 1 {
			cntUpvote++
		} else {
			cntDownvote++
		}
	}
	if cntDownvote-cntUpvote == 0 {
		item.UpVotePercentage = 0
		item.Score = 0
		err = ir.UpdateItem(item)
		if err != nil {
			return err
		}
		return nil
	}
	item.UpVotePercentage = int(100 * cntUpvote / (cntDownvote + cntUpvote))
	item.Score = cntUpvote - cntDownvote
	err = ir.UpdateItem(item)
	if err != nil {
		return err
	}
	return nil
}

func (ir *ItemsRepo) AddVoteByID(id string, vote vote.Vote) error {
	item, err := ir.GetItemByID(id)
	if err != nil {
		return err
	}
	add := true
	for idx, curvote := range item.Votes {
		if curvote.UserID == vote.UserID {
			item.Votes[idx].Vote = vote.Vote
			err = ir.UpdateItem(item)
			if err != nil {
				return err
			}
			add = false
			break
		}
	}
	if add {
		item.Votes = append(item.Votes, vote)
		err = ir.UpdateItem(item)
		if err != nil {
			return err
		}
	}
	return nil
}

func (ir *ItemsRepo) DeleteItemByItemID(id string) error {
	if !bson.IsObjectIdHex(id) {
		return ErrNoValidID
	}
	idB := bson.ObjectIdHex(id)
	err := ir.data.Collection(collectionName).Remove(bson.M{"_id": idB})
	if err == mgo.ErrNotFound {
		return fmt.Errorf("item with this ID %s does not exist, error: %w", id, err)
	} else if err != nil {
		return fmt.Errorf("error when updating the item to the database, error: %w", err)
	}
	return nil
}

func (ir *ItemsRepo) GetAllItemsByUserName(userName string) []*Item {
	itemsUser := []*Item{}
	for _, item := range ir.GetAllItems() {
		if item.Author.UserName == userName {
			itemsUser = append(itemsUser, item)
		}
	}
	return itemsUser
}
