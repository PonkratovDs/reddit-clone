package items

import "gopkg.in/mgo.v2"

type DatabaseHelper interface {
	Collection(name string) MyCollection
}

type mongoDatabase struct {
	db *mgo.Database
}
type mongoCollection struct {
	coll *mgo.Collection
}

type mongoSession struct {
	sess *mgo.Session
}

func (md *mongoDatabase) Collection(colName string) MyCollection {
	collection := md.db.C(colName)
	return &mongoCollection{coll: collection}
}

func (mc *mongoCollection) Insert(docs interface{}) error {
	err := mc.coll.Insert(docs)
	return err
}

func (mc *mongoCollection) Find(query interface{}) MyQuery {
	q := mc.coll.Find(query)
	return q
}

func (mc *mongoCollection) Update(selector interface{}, update interface{}) error {
	err := mc.coll.Update(selector, update)
	return err
}

func (mc *mongoCollection) Remove(selector interface{}) error {
	err := mc.coll.Remove(selector)
	return err
}

func NewDatabaseHelper(db *mgo.Database) DatabaseHelper {
	return &mongoDatabase{
		db: db,
	}
}
