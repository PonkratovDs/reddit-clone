// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	items "redditclone/pkg/items"

	mock "github.com/stretchr/testify/mock"
)

// MyCollection is an autogenerated mock type for the MyCollection type
type MyCollection struct {
	mock.Mock
}

// Find provides a mock function with given fields: query
func (_m *MyCollection) Find(query interface{}) items.MyQuery {
	ret := _m.Called(query)

	var r0 items.MyQuery
	if rf, ok := ret.Get(0).(func(interface{}) items.MyQuery); ok {
		r0 = rf(query)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(items.MyQuery)
		}
	}

	return r0
}

// Insert provides a mock function with given fields: docs
func (_m *MyCollection) Insert(docs interface{}) error {
	ret := _m.Called(docs)

	var r0 error
	if rf, ok := ret.Get(0).(func(interface{}) error); ok {
		r0 = rf(docs)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Remove provides a mock function with given fields: selector
func (_m *MyCollection) Remove(selector interface{}) error {
	ret := _m.Called(selector)

	var r0 error
	if rf, ok := ret.Get(0).(func(interface{}) error); ok {
		r0 = rf(selector)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Update provides a mock function with given fields: selector, update
func (_m *MyCollection) Update(selector interface{}, update interface{}) error {
	ret := _m.Called(selector, update)

	var r0 error
	if rf, ok := ret.Get(0).(func(interface{}, interface{}) error); ok {
		r0 = rf(selector, update)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
