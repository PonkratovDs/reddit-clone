package vote

type Vote struct {
	UserID string `schema:"user,required" json:"user"`
	Vote   int    `schema:"vote,required" json:"vote"`
}
