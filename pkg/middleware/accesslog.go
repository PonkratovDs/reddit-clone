package middleware

import (
	"net/http"
	"time"

	"go.uber.org/zap"
)

type LoggerMiddleware struct {
	Logger *zap.SugaredLogger
}

func (lm *LoggerMiddleware) AccessLog(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		lm.Logger.Infow("New request",
			"method", r.Method,
			"remote_addr", r.RemoteAddr,
			"url", r.URL.Path,
			"time", time.Since(start),
		)
	})
}
