package middleware

import (
	"context"
	"net/http"
	"redditclone/pkg/session"
	"strings"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

func AuthName(handlerName string) bool {
	switch handlerName {
	case "AddPost", "AddCommentByPostID", "DeleteCommentByPostAndCommentID", "MakeUpvoteForPostByID", "MakeDownvoteForPostByID", "DeletePostByPostID":
		return true
	}
	return false
}

type AuthenticationMiddleware struct {
	Logger *zap.SugaredLogger
	Sm     *session.SessionsManager
}

func (amw *AuthenticationMiddleware) Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlerName := "NO_NAME"
		if route := mux.CurrentRoute(r); route != nil {
			routeName := route.GetName()
			if routeName != "" {
				handlerName = routeName
			}
		}
		if handlerName == "NO_NAME" {
			amw.Logger.Info("error when defining the handler name")
			http.Redirect(w, r, "/", 302)
			return
		}

		if r.Header.Get("Authorization") == "" {
			if AuthName(handlerName) {
				amw.Logger.Info("token with authorization did not come")
				http.Redirect(w, r, "/", 302)
				return
			}
			next.ServeHTTP(w, r)
			return
		}
		tokenString := strings.TrimSpace(r.Header.Get("Authorization"))
		tokenString = strings.TrimPrefix(tokenString, "Bearer ")
		sess, err := amw.Sm.GetSessionFromJWT(tokenString)
		if err != nil {
			amw.Logger.Error(err)
			http.Redirect(w, r, "/", 302)
			return
		}
		ctx := context.WithValue(r.Context(), session.SessionKey, sess)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
