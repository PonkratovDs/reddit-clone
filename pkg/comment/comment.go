package comment

import (
	"redditclone/pkg/user"
)

type Comment struct {
	Author  user.User `schema:"author,required" json:"author"`
	Body    string    `schema:"body,required" json:"body"`
	Created string    `schema:"created,required" json:"created"`
	ID      string    `schema:"id, required" json:"id"`
}

type UnmarshalComment struct {
	Comment string `json:"comment"`
}
